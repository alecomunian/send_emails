README
===================

This is the readme file of the script ``send_emails.py``.

|
What is this repository for?
--------------------------------------------

* This script is useful to send to a list of receivers (provided into a XLSX file)
  some personalized attachments.
* Version 0.1

|
How do I get set up?
-----------------------------------

* To run this script you will need a recent Python 3 installation. One practical option
  is to install Anaconda from `https://www.anaconda.com <https://www.anaconda.com>`_.
* Then, you can use the editor *Spyderlib* (can be installed from Anaconda) to open, edit and run the script.
* The script uses the python module ``email`` and ``smtplib``, which in general should be already part of your
  Python installation. If not, they can be easily installed with the command ``pip`` or ``conda``, or from
  the Anaconda package manager.
* To use the script, first set up the variables ``apegeo_date``,
  ``dry_run``, ``pdf_root_name``, ``attach_dir`` and
  ``address_list``. Their meaning should be straightforward, and they
  are documented in the script.
  
* Then, you can run in from Spyderlib using <F5> or *run* from the menu.
  Alternatively, you can use the command line like::

    python3 send_emails.py
  
* When running, the script should ask for a couple of additional
  information, and print out the names of the receivers.
|
Who do I talk to?
----------------------------------

* For any question, please ask to Alessandro Comunian AtT unimi Dot it
  

