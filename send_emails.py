#!/usr/bin/env python3
"""
:This file:

    `send_emails.py`

:Purpose:

    A simple script to read from an XLSX file name, surname and e-mail
    address and send to the corresponding contact a PDF file
    (attendance report).

:Usage:

    From the command line, enter into the directory where the script is located
    and type:

        python3 send_emails.py

    Alternatively, you can use <F5> or the "Run" command if you are 
    using Spyderlib.

:Parameters:

    See in the script the section "MODIFY THESE PARAMETERS" to set up the 
    parameters to be provided.

:Version:

    0.1 , 2021-06-01 :
        * First revised version.

:Authors:

    Alessandro Comunian

.. warning::

    Check that the name of the file is properly formed, for example by
    running first a "dry run" and checking the output.
    Double check for example the suffix of the file, which is assigned
    to the variable "filename".

.. note::
    1) To double check if the script worked properly, you can 
       add your address at the end of the XLSX file and a "fake" attachment
       (with the proper numbering...) and run it like that.

"""

import smtplib, ssl
import os
import pandas as pd
import getpass

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# ==============================================================
# MODIFY THESE PARAMETERS - START
# ==============================================================

# The receiver ID to start with.
# A negative value (default) allows to send an e-mail to all the
# names in the list.
# Suggestion: try with the option "dry_run" turned ON to check the
# correctness of the selected ID.
min_id = -1

# Dry run: set to True if you only want to test if names and folders
# are formed correctly. No e-mail will be sent if set to True
dry_run = True

# This is the date of the ApeGeo to be put in the subject, for example "30 aprile 2020"
apegeo_date = "20 ottobre 2023"
# The root name of the PDF files
pdf_root_name = "Attestato_Apegeo_20ottobre23-"
# Directory where the XLSX file with the list and the attachments
attach_dir = os.path.join(".", "attestati")
# The name of the XLSX file containing the names and the email addresses.
address_list = os.path.join(attach_dir, "database_attestati.xlsx")
#address_list = os.path.join(attach_dir, "fake.xlsx")

# ==============================================================
# MODIFY THESE PARAMETERS - END
# ==============================================================


# Sender e-mail
sender_email = input("Please enter the sender e-mail: ")
# Ask here for the sender email account password.
password = getpass.getpass("Type your password and press enter:")

# Read the file containing the list of the participants
addr = pd.read_excel(address_list)

# Loop all over the lines of the spreadsheet
for i, row in addr.iterrows():
    # If needed, start from a selected line and go on...
    if i >= min_id-1:
        # Set here the info for the body
        #name = row["Nome"].rstrip()#.capitalize()+ " " + row["cognome"].rstrip()
        name = row["Nome"].rstrip() + " " + row["Cognome"].rstrip()
        # surname = row["Cognome"].rstrip().capitalize()
        receiver_email = row["Email"].replace(" ", "")

        # Print some output/debug info
        print('Sending to "{0}"'.format(name))
        print("    email: {0}".format(receiver_email))
        print("    ID: {0}".format(i+1))

        # Create a multipart message and set headers
        message = MIMEMultipart()
        message["From"] = sender_email
        message["To"] = receiver_email
        message["Subject"] = "invio attestato ApeGeo del {0}".format(apegeo_date)    
        message["Bcc"] = receiver_email  # Recommended for mass emails
        
        # Add body to email
        body = """\
Gentile {0},

in allegato l'attestato di partecipazione al seminario "Apegeo, aperitivi scientifici coi piedi per Terra!" del {1}.

Cogliamo l'occasione per ricordarle che i video delle conferenze verranno resi disponibili sul canale YouTube del Dipartimento al link: http://bit.ly/dipterraYT

Per maggiori informazioni, visita il sito apegeo.unimi.it 

Un caro saluto e a presto,

il Team ApeGeo

------
Dipartimento Scienze della Terra A. Desio - Orientamento e Promozione

Facebook - https://m.facebook.com/geoscienze.unimi/
Instagram - https://www.instagram.com/geoscienze_unimi/
Youtube - https://www.youtube.com/channel/UCvTQMcwmL-9LoiOBL5NIldA/featured

        """.format(name, apegeo_date)
        message.attach(MIMEText(body, "plain"))
        
        # REMEMBER: Check here the suffix of the attached files.
        filename = os.path.join(attach_dir, "{0}{1}_signed.pdf".format(pdf_root_name, i+1))
        print("    attached file: {0}".format(filename))
        
        # Open PDF file in binary mode
        with open(filename, "rb") as attachment:
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "pdf")
            part.set_payload(attachment.read())
        
        # Encode file in ASCII characters to send by email    
        encoders.encode_base64(part)
            
        # Add header as key/value pair to attachment part
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {filename}",
        )
            
        # Add attachment to message and convert message to string
        message.attach(part)
        text = message.as_string()

        # Log in to server using secure context and send email
        context = ssl.create_default_context()
        if not dry_run:
            # This will login to the served and send e-mails
            # NOTE: Logging in only once does not allow to send many messages.
            #       Therefore, we login each time for each receiver.
            with smtplib.SMTP_SSL("smtp.unimi.it", 465, context=context) as server:
                server.login(sender_email, password)
                server.sendmail(sender_email, receiver_email, text)
            print("    Done.")
        else:
            print("    Done (dry run).")
        
        
        
        
        
        
        
        
